lazy val refVersion = "0.1"
lazy val refName = "kali-hacker-rank"
lazy val outputJarName = refName + "-assembly-" + refVersion + ".jar"

crossPaths := false

test in assembly := {}

name := refName

version := refVersion

scalaVersion := "2.11.7"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

resolvers += "webjars" at "http://webjars.github.com/m2"

scalacOptions ++= Seq(
    "-deprecation",
    "-unchecked",
    "-feature",
    "-encoding",
    "UTF-8",
    "-Xlint"
    // "-optimise"   // this option will slow your build
)


parallelExecution in Test := false

libraryDependencies ++= {
    Seq(
        "org.scalatest" %% "scalatest" % "2.2.4",
        "org.mockito" % "mockito-all" % "1.9.5"
    )
}

libraryDependencies ~= {
    _ map {
        case m => m.exclude( "com.typesafe.scala-logging", "scala-logging_2.11" )
    }
}