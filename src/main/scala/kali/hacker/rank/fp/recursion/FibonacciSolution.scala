package kali.hacker.rank.fp.recursion

object FibonacciSolution {

    def fibonacci( x: Int ): Int = {
        if ( x <= 1 ) {
            0
        } else if ( x == 2 ) {
            1
        } else {
            fibonacci( x - 1 ) + fibonacci( x - 2 )
        }
    }

    def main( args: Array[ String ] ) {
        /** This will handle the input and output **/
        //println(fibonacci(readInt()))

        println( fibonacci( 1 ) )
        println( fibonacci( 2 ) )
        println( fibonacci( 3 ) )
        println( fibonacci( 4 ) )
        println( fibonacci( 5 ) )
    }


}