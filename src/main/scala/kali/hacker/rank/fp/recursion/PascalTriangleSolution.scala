package kali.hacker.rank.fp.recursion

object PascalTriangleSolution {

    def main( args: Array[ String ] ) {
        pascal( scala.io.StdIn.readLine( ).trim.toInt )
    }

    def pascal( n: Int ): Unit = {
        if ( n == 1 ) {
            val x = List( 1 )
            line( x )
        } else {
            pascal( 1 )
            ( 1 until n ).foldLeft( List[ Int ]( 1, 1 ) ) {
                ( o, i ) =>
                    line( o )
                    1 +: o.sliding( 2, 1 ).map( _.sum ).toList :+ 1
            }
        }
    }

    def line( series: List[ Int ] ): Unit = {
        println( series.mkString( " " ) )
    }

}