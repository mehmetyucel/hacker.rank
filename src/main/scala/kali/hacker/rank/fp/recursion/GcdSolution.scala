package kali.hacker.rank.fp.recursion

object GcdSolution {

        def gcd(x: Int, y: Int): Int =
        {
            // You only need to fill up this function
            // To return the value of the GCD of x and y
            b( x, y)
        }

        def b( n : Int, q: Int ) : Int= {
            val r1 = n % q
            val q1 = n / q
            if( r1 == 0)
                q
            else
                b(q, r1)
        }

        /**This part handles the input/output. Do not change or modify it **/
        def acceptInputAndComputeGCD(pair:List[Int]) = {
            println(gcd(pair.head,pair.reverse.head))
        }

        def main(args: Array[String]) {
            /** The part relates to the input/output. Do not change or modify it **/
            acceptInputAndComputeGCD(readLine().trim().split(" ").map(x=>x.toInt).toList)

        }



}
