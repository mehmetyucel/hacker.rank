package kali.hacker.rank.fp.recursion

import scala.collection.immutable.IndexedSeq

class BaseType( val values: Seq[String] )

case class T( override val values : Seq[String]) extends BaseType(values)
case class F( override val values : Seq[String]) extends BaseType(values)

object SierpinskiTriangleSolution extends App {
    case class Point( x: Int, y:Int )

    def drawBoard( size : Int ) = {
        for (i <- 1 to size) yield "_" * ( size*2 )
    }

    def paint( point: Point, board: Seq[String]) = {
        multi( board( point.x).charAt(point.y), '1')

    }

    def multi(test: Char, original: Char ) : Char = {
        if( test == '1' )
            '1'
        else
            original
    }




    drawBoard(32) foreach println
    val b = drawBoard(32)
    paint( Point(3,5), b) foreach println
}


object SierpinskiTriangleSolution5 extends App {
//    type F =  Seq[ String ]
//    type T = Seq[String]

    def filler( height: Int, length: Int = 1): F = {
        val k = for (i <- 1 to height) yield "_" * length
        F(k)
    }

    def triang( height: Int ): T = {
        val t = for( i <- 1 to (height*2) by 2 ) yield line( i, (height*2)-1)
        T(t)
    }


    def line( row: Int, width: Int ): String = {
        val size =  (width - row ) / 2
        ("_" * size) + ( "1" * row ) + ( "_" * size )
    }


    def multi(test: Char, original: Char ) : Char = {
        if( test == '1' )
            '1'
        else
            original
    }

    def paste( a: Seq[BaseType] ) : Seq[String] = {
        if(a.length == 2)
            (a(0).values, a(1).values).zipped.map( _ + _)
        else
            paste( Seq( a.head,  new BaseType( paste( a.tail )) ) )
    }

    // 0
    //triangle( 64 ) foreach println
    // 1
    paste( Seq(filler(16,16), triang( 16 ), filler(16,16)) ) foreach println
    paste( Seq(triang(16), filler(16,1), triang( 16 )) ) foreach println
    // 2
    paste( Seq(filler(8,8),filler(8,8),filler(8,8),triang( 8 ),filler(8,8),filler(8,8),filler(8,8)) ) foreach println
    paste( Seq(filler(8,8),filler(8,8),triang( 8 ),filler(8,1),triang( 8 ),filler(8,8),filler(8,8)) ) foreach println
    paste( Seq(filler(8,8),triang( 8 ),filler(8,8),filler(8,1),filler(8,8),triang( 8 ),filler(8,8)) ) foreach println
    paste( Seq(triang( 8 ),filler(8,1),triang( 8 ),filler(8,1),triang( 8 ),filler(8,1),triang( 8 )) ) foreach println
    // 3
    paste( Seq(filler(4,28), triang( 4 ), filler(4,28)) ) foreach println
    paste( Seq(filler(4,24), triang( 4 ), filler(4,1),triang(4), filler(4,24)) ) foreach println
    paste( Seq(filler(4,20), triang( 4 ), filler(4,9),triang(4), filler(4,20)) ) foreach println
    paste( Seq(filler(4,16), triang( 4 ), filler(4 ), triang(4), filler(4),triang(4), filler(4), triang(4), filler(4,16)) ) foreach println

    println("here")
    println("here")

    /*
    println( filler(3,3).isInstanceOf[T])
    println(filler(3,3).isInstanceOf[F])
    println(triang(3).isInstanceOf[T])
    println(triang(3).isInstanceOf[F])
*/

    val cnt = 2
    val max = 64

    val lineLength = max / Math.pow(2 , cnt+1).toInt
    val blockCount = Math.pow(2, cnt+1).toInt - 1
    val midPosition = blockCount / 2
    val lineCount = (max / 2) / lineLength

    println( lineLength )
    println( blockCount )
    println( midPosition )

    for( i <- 0 until lineCount ) {
        val firstLine: IndexedSeq[ BaseType ] = for ( j <- 0 until blockCount )
            yield
                if ( j == midPosition )
                    triang( lineLength )
                else
                    filler( lineLength, lineLength )

        paste( firstLine ) foreach println
    }



}

object SierpinskiTriangleSolution4 extends App{
    def line(num: Int, width: Int): String = {
        def underscores(x: Int): String = "_" * x
        def ones(x: Int): String = "1" * x
        def underscoreWidth: Int = (width - num) / 2
        underscores(underscoreWidth) + ones(num) + underscores(underscoreWidth)
    }

    def triangle(height: Int): Seq[String] = {
        require(height >= 1)
        def width: Int = height * 2 - 1
        for (i <- 1 to height) yield line(i * 2 - 1, width)
    }

    def emptySquareBox(height: Int): Seq[String] = {
        for (i <- 1 to height) yield "_" * height
    }
    def emptyLongBox(height: Int): Seq[String] = for (i <- 1 to height) yield "_"

    def sierpinskiTriangle(i: Int, height: Int): Seq[String] = {
        i match {
            case 0 => triangle(height)
            case _ =>
                def newHeight = height / 2
                (emptySquareBox(newHeight), sierpinskiTriangle(i - 1, newHeight), emptySquareBox(newHeight)).zipped.map(_ + _ + _) ++
                (sierpinskiTriangle(i - 1, newHeight), emptyLongBox(newHeight), sierpinskiTriangle(i - 1, newHeight)).zipped.map(_ + _ + _)
        }
    }
    sierpinskiTriangle(3, 32) foreach println
}

object SierpinskiTriangleSolution3 {
    def sierpinski(n: Int) {
        def star(n: Long) = if ((n & 1L) == 1L) "*" else " "
        def stars(n: Long): String = if (n == 0L) "" else star(n) + " " + stars(n >> 1)
        def spaces(n: Int) = " " * n
        ((1 << n) - 1 to 0 by -1).foldLeft(1L) {
            case (bitmap, remainingLines) =>
                println(spaces(remainingLines) + stars(bitmap))
                (bitmap << 1) ^ bitmap
        }
    }

    def main( args: Array[ String ]): Unit = {
        sierpinski(3)
    }
}


object SierpinskiTriangleSolution2 {

    def main( args: Array[ String ] ) {
        //        drawTriangles(readInt())
        drawTriangles( 1, 64, 32 )
        drawTriangles( 2, 64, 32 )
    }

    def drawTriangles( n: Int, cols: Int, rows: Int ) {
        //Draw the N'th iteration of the fractal as described
        // in the problem statement
        for ( i <- 1 to rows/n ) {
            println( drawLine( i, "_" * ((cols/n) - 1), (cols/n)-1 ) )
        }
    }


    def drawLine( line: Int, x: String, cols: Int ): String = {
        val mid = cols / 2
        val startFrom = mid - ( line - 1 )
        val finishAt = mid + ( line - 1 )
        ( startFrom to finishAt ).foldLeft( x ) {
            ( o, i ) => o.updated( i, '1' )
        }
    }


    def invert( c: String ): String = {
        if ( c == "_" ) {
            "1"
        }
        else {
            "_"
        }
    }

}

/*

4
0 0
0 1
1 1
1 0
 */

