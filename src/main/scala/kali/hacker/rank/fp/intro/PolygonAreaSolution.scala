package kali.hacker.rank.fp.intro

import scala.collection.immutable.IndexedSeq

object PolygonAreaSolution {
    def main( args: Array[ String ] ): Unit = {
        val dataCount = scala.io.StdIn.readLine( ).trim.toInt
        val data = readData( dataCount )
        // println( data )

        val b = (data :+ data.head ).foldLeft( Tuple3[ Option[Int], Option[Int], Double ]( None, None, 0d)  ) {
            (o, i) => if( o._1.isEmpty ) {
                ( Some(i._1), Some(i._2), 0)
            } else {
                val sum =  (i._2 * o._1.get) - (i._1 * o._2.get)
                // val sum = Math.sqrt( Math.pow( (i._2 - o._2.get), 2 ) + Math.pow( (i._1 - o._1.get), 2 ))
                ( Some(i._1), Some(i._2), o._3 + sum )
            }
        }

        println( Math.abs(b._3 / 2) )
    }

    def readData( count: Int ): IndexedSeq[ (Int, Int) ] = {
        for( i <- 1 to count ) yield scala.io.StdIn.readLine( ).trim.split( " " ) match {
            case Array(a, b) => (a.toInt, b.toInt )
        }
    }



}

/*

4
0 0
0 1
1 1
1 0
 */

