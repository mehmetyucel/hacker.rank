package kali.hacker.rank.fp.intro

object FunctionOrNotSolution {

    def main( args: Array[ String ] ): Unit = {

        val testCaseCount = readCaseCount
        val data = ( 1 to testCaseCount ).map( testCase => {
            val dataCount = readDataCount
            readData( dataCount )
        })

        //println( data )

        data.foreach( samples => {
            if( isFunction( samples ) )
                println("YES")
            else
                println("NO")
        })

    }

    def isFunction( sampleList : Iterable[(Int, Int)]) : Boolean = {
        sampleList.foldLeft( Map[Int, Int]() ) {
            (o, i) => if( o.contains(i._1) && o(i._1) != i._2 ) return false else o + (i._1 -> i._2)
        }
        true
    }


    def readCaseCount = {
        scala.io.StdIn.readLine( ).trim.toInt
    }

    def readDataCount = {
        scala.io.StdIn.readLine( ).trim.toInt
    }

    def readData( count: Int ) = {
        for( i <- 1 to count ) yield scala.io.StdIn.readLine( ).trim.split( " " ) match {
            case Array(a, b) => (a.toInt, b.toInt )
        }
    }


}



/**




3
3
1 1
2 2
3 3
4
1 2
2 4
3 6
4 8
3
1 1
2 3
2 5
*/